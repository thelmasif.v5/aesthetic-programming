## MiniX 6 - Object abstraction 

Runme: https://thelmasif.v5.gitlab.io/aesthetic-programming/MiniX6/

![](Bunnyimg2.jpg)

### Ideation process
This week's assignment was to make a simple mini game implementing class based objects. I remember Winnie saying before this week that this was going to be the hardest MiniX yet and that if we were able to do this one, we would be able to finish this course. And she wasn’t lying, I was really struggling making this week's MiniX work. I started again and again with ideas of minigames I thought would have fairly simple code then ended up being much harder than anticipated. Some of my ideas included the 2048 game, 1010! game that is like a tetris puzzle. 

Then my final idea got the inspiration from the “dinosaur game” from when the internet connection is lost in google chrome. I looked up inspiration on how I could do that and conveniently Daniel Shiffman from The Coding Train had done a video on how to make a similar game. I learned a lot by following his tutorial and it gave me a good understanding of classes and how to add pictures etc.

The way the game works is that when you run it you click the screen to start and then you use the spacebar to make the bunny jump over the bushes. Sometimes you have to wait a little bit for the bushes to show up and that is because I had to lower the random value to make it easier so a bunch of bushes don’t suddenly show up in a row that you can’t jump over. 

### Code
I created the background, the bunny and the bush in a program called canva. I got that idea because I remembered that in canva you can easily resize you designs with pixels so I could adjust it to the game so it wouldn’t look stretched. Like the background was Then I saved the bush and the bunny as a png with a transparent background and added it to my classes. In my code there are two seperate classes for the bunny and the bushes. 

The game objects are the bunny and the bushes. The player has control over the jumping bunny and then the computer generates the bushes showing up according to my ”random” syntax. The methods for the behaviour of the bunny is the jumping along with the gravity and I had to make it so the bunny can’t jump again until it has touched the ground. I did that with a conditional statement. To do the collide detection for when the bunny hits the bush I had to add a new library into my index.html file. This library is called p5.collide2D and was created by user Bmoren. From that I used “collideRectRect” but then to make it a little more specific on when the bush hits the bunny I made it into collideCircleCircle so it’s a circle around the bunny that is detected. 

I think I learned the most out of this MiniX than any of the previous we’ve done. I also think the functions are very useful for a lot of things. For example learning the preLoad function to upload any image to the sketch, also with doing classes to isolate the properties and behavior of each object. 

### Complications
The thing I spent the most time on was trying to find out how to make a “retry” or a “try again” button so you don’t have to refresh the page. Unfortunately I wasn’t able to figure out how to make it work fully, the retry button comes on when the bunny hits the bush but in order to make it refresh using the button you have to click it until the bunny isn’t touching the bush anymore. I tried looking at the tofu example code to see if there was a solution but when I saw that it didn’t have a button like I thought it had so I decided it was alright to have it like this. I also had complications with putting a score count up in the corner like I wanted but I think that is not the most important thing even though it would have made the game more fun to play. 

### Cultural context
The dinosaur game in google chrome is known by so many people around the world. For me it’s quite nostalgic and reminds me of middle school computer class when the internet connection would sometimes be interrupted. The game is very fun to play to kill time while you wait for the internet to come back. I think what is charming about the game is the simplicity. It’s easy to play and doesn’t require much skill. I looked up how long the dino game has existed and was surprised to see that it was actually released in 2014. I thought it was much older judging by the retro, pixelated look. That fact is just to show that the game was created with simplicity in mind and I think it’s genius. 

### References
https://youtu.be/l0HoJHc-63Q
https://en.wikipedia.org/wiki/Dinosaur_Game
