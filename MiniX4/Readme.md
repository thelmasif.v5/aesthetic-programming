# MiniX 4 - Data Capture
RunMe: https://thelmasif.v5.gitlab.io/aesthetic-programming/MiniX4/
![](Cookie2.jpg)
![](Cookie.jpg)

### Cookies
Cookie monster is a famous character from a kids tv show called “Sesame Street” and is known for its love for cookies. In my MiniX about data capture I decided to do a little play with words as website cookies and the edible cookies have the same name. 
But what are the kinds of cookies we’ve all been asked if we want to accept on almost every website we visit? What happens when you accept them? I have to admit that I did not know the answer to these questions prior to this course. Cookies are in fact small text files sent by the website you're visiting to the computer or device you're using. If you accept them, they will be stored on the web browser of your device. Cookies can then track and collect data from your browser, sending that data back to the website owner. 
To sum it up, cookies can oftentimes be helpful but in some cases it’s better to not accept them. For example on unencrypted websites where there is no security to protect your data. Also cookies may take up some disk space on your computer which I didn’t know. 

### Concept
The concept is that you see a pop up window where you are asked to accept cookies, like in many of those website pop up window there is no reject button because many websites don’t allow you to use their website unless you accept the cookies. Once you click accept cookie monster appears and accuses you of stealing his cookies and that he now has all your data. I really wanted to make the capture webcam feature pop up in a little window in front of the cookie monster but I didn’t seem to be able to put an image syntax on top of an image syntax. Very frustrated after trying for hours I accepted the fact that the webcam was just going to be at the front page. But that is also fine, like you are being watched without even accepting the cookies. Who knows, maybe someone could access your webcam without you knowing. While doing this project I also thought it was scary how simple it was to program web camera features and makes me think about if it is as easy to hack it.

### Code
I used many new syntaxes for this week's MiniX, firstly I used mousePushed and mousePressed to be able to click the button. I also played around with some shapes and elements to make the front page and the cookie from scratch. Then I made and angry picture of the Cookie monster and used the preload function to upload it. Lastly I used webcam capture, that like I mentioned I wasn’t able to put in the right place but now I’m familiar with the syntax and can use it in future projects.

### Data capture
The act of data capturing is hugely important for example for social media and therefore society. It tracks like, click, scroll to personalise the algorithm to your liking to make you stay on the app for as long as possible. This can be both useful and harmful. I am very interested in algorithms on social media but it can also be very scary that the thing you were talking about to your friend pops up as an ad on facebook without you even googling it. That means your data is being captured in more ways than we even know of. 

Also to connect this to our other course about the ownership of our data online. Who are the owners of our data after it’s collected? Is it the website or do we have the rights to own it? We also talked about data scraping which is when information is taken from public websites without the illegal element of hacking. It’s also in the “Christian Ulrik Andersen and Geoff Cox” text about the implications of data capture they mention about “who has ownership over our data after we die?” Which I think is really interesting to think about.

### References
https://p5js.org/reference/#/p5/createButton
https://p5js.org/examples/hello-p5-interactivity-1.html
https://p5js.org/reference/#/p5/image
https://p5js.org/reference/#/p5/createCapture
https://us.norton.com/internetsecurity-privacy-should-i-accept-cookies.html
Christian Ulrik Andersen and Geoff Cox, eds., A Peer-Reviewed Journal About Datafied Research 4, no. 1 (2015)

