let r = 1;
let g = 1;
let b = 1;

//vertical and horizontal points
let vp = 0;
let hp = 0;

function setup() {
  createCanvas(800, 800);
  random(100, 255);
  background(50);
  frameRate(50);
}

function draw() {
  noStroke();
  r = random(0, 255);
  g = random(100, 255);
  b = random(150, 255);
  fill(r, g, b, 100);
  circle(40 * hp, 60 * vp, 78);

  //filling out the whole canvas
  hp = hp + 1;
  if (hp > 20) {
    vp = vp + 0.66;
    hp = -1;
  }
}
