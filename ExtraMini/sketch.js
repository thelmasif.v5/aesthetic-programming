let x;

function setup() {
  createCanvas(400, 400);

}

function draw() {
  background(90, 200, 240);
  if ((mouseX > 90) && (mouseX < 330) && (mouseY > 190) && (mouseY < 398)){
    translate(random(-5,5),random(-5,5));
}
  if (mouseIsPressed == true) {
  fill(0);
  //volcano
  noStroke();
  fill(100);
  beginShape();
  vertex(160, 200);
  vertex(240, 200);
  vertex(350, 398);
  vertex(50, 398);
  endShape();

  //lava
  fill(200, 100, 0);
  rect(150, 200, 100, 20, 40);
  rect(175, 200, 30, 90, 50);
  rect(160, 200, 30, 35, 50);
  rect(200, 200, 30, 45, 50);

  push();
  translate(100, 100);
  rotate(PI / 7);
  rect(95, 65, 20, 60, 50);
  translate(100, 100);
  rotate(PI / -3.5);
  rect(71, 15, 30, 60, 50);
  pop();
  
  push();
  fill(200,0,0)
  translate(100, 100);
  rotate(PI / 7);
  rect(120, -65, 40, 100, 50);
  translate(100, 100);
  rotate(PI / -5);
  rect(50, -135, 40, 110, 50);
  rotate(PI / -3);
  rect(50, -50, 40, 100, 50);
  circle(190,70,25)
  pop();
  
  push();
  fill(200,100,0)
  translate(100, 100);
  rotate(PI / 7);
  rect(130, -55, 20, 85, 50);
  translate(100, 100);
  rotate(PI / -5);
  rect(60, -125, 20, 90, 50);
  rotate(PI / -3);
  rect(60, -40, 20, 90, 50);
  circle(190,70,15)
  pop();
  
 }  
  else {
      noStroke();
  fill(100);
  beginShape();
  vertex(160, 200);
  vertex(240, 200);
  vertex(350, 398);
  vertex(50, 398);
  endShape();

  //lava
  fill(200, 100, 0);
  rect(150, 200, 100, 20, 40);
  rect(175, 200, 30, 90, 50);
  rect(160, 200, 30, 35, 50);
  rect(200, 200, 30, 45, 50);

  push();
  translate(100, 100);
  rotate(PI / 7);
  rect(95, 65, 20, 60, 50);
  translate(100, 100);
  rotate(PI / -3.5);
  rect(71, 15, 30, 60, 50);
  pop();
  
  }
  }
