function setup() {
  createCanvas(450, 400);
   background(255,182,193);
  
  
}
function draw() {
  noStroke();
  if (mouseIsPressed === true) {
    //head
fill(255, 240, 0);
  ellipse(180, 180, 200, 200);
  noStroke();
    
      //eyes
  fill(0,0,0)
  ellipse(137, 170, 40, 40);
  fill(0,0,0)
  ellipse(220, 170, 40, 40);
  fill(255,240,0)
  ellipse(137, 170, 33, 33);
  fill(255,240,0)
  ellipse(220, 170, 33, 33);
  fill(255,240,0)
  rect(100, 165, 150, 40)
    
    //mouth
     fill(0,0,0)
  ellipse(200, 210, 20, 20);
  fill(0,0,0)
  ellipse(200, 225, 20, 20);
    
    fill(255,240,0)
  ellipse(200, 210, 14, 14);
  fill(255,240,0)
  ellipse(200, 225, 14, 14);
    fill(255,240,0)
  ellipse(190, 217, 20, 20);
    fill(255,240,0)
    rect(174, 190, 20, 50)
    
    //heart
    fill(255,20,100)
     ellipse(279.5,210,31.5,31.5);
    ellipse(250.5,210,31.5,31.5);
    triangle(263,250,235,216,295,216);

    
    //peace sign
push()
    fill(255,220,0)
    rotate(0.4/2)
    rect(390, 140, 20, 70, 100);
    
  rotate(-0.5/1)
    fill(255,220,0);
    rect(220,305,20,70,100);
pop()
    
  fill(255, 220, 0);
  ellipse(360, 305, 70, 70);
  fill(255, 220, 0);
  ellipse(350, 305, 70, 70);
  noStroke();
    
    fill(255,240,0);
    rect(376,270,20,40,100);
    fill(255,240,0);
    rect(355,270,20,40,100);
    
    fill(255,240,0);
    rect(315,308,50,20,100);
    
      }  else {
   background(255,182,193);
  //head
  fill(255, 240, 0);
  ellipse(180, 180, 200, 200);
  noStroke();

  //mouth
  
  fill(0, 0, 0);
  ellipse(180, 190, 100, 100);
  fill(255,240,0)
  ellipse(180, 190, 90, 90);
  fill(255,240,0)
  rect(130, 120, 100, 80)
  
  //eyes
  fill(0,0,0)
  ellipse(145, 150, 15, 40);
  
  fill(0,0,0)
  ellipse(210, 150, 15, 40);
    
  }

}
