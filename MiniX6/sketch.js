let screen = 0;
let bunny;
let uImg;
let tImg;
let bImg;
let bushes = [];
let restartButton

function preload(){
  uImg=loadImage('bunny.png')
 tImg=loadImage('bush.png')
bImg=loadImage('background.png')
 } 
function setup() {
  createCanvas(800, 450);
    bunny= new Bunny();
 
}

function keyPressed(){
  if(key == ' ') {
    bunny.jump();
 }
}
function draw() { 
  if(random(1) < 0.006){
    bushes.push(new Bush());
  collideRectRect(200, 200, 100, 150, mouseX, mouseY, 50, 75);
  
}
  background(bImg);
    for(let t of bushes){
    t.move();
    t.show();
      if(bunny.hits(t)){
      console.log('game over');
      noLoop();
      restartButton = createButton("RESTART")
      restartButton.position(width/2-50,height/2)
      restartButton.mousePressed(restartGame)
        }
      }
  
  function restartGame(){
  restartButton.remove();
  loop() 
  
}
  bunny.show();
  bunny.move();  
  
}