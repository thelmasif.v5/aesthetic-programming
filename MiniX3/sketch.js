
function setup() {
  createCanvas(400, 400,WEBGL);
}

function draw() {
  background(255);
  
  noStroke();
  let locX = mouseX - height / 2;
  let locY = mouseY - width / 2;

  ambientLight(50);
  directionalLight(255, 0, 0, 2, 2, 0);
  pointLight(0, 0, 255, locX, locY, 100);

  for (let x = -160; x <= width; x += 80 )
  circle(x,-160,60)
  for (let x = -160 ; x <= width; x += 80 )
  circle(x,-80,60)
  for (let x = -160; x <= width; x += 80 )
  circle(x,0,60)
  for (let x = -160; x <= width; x += 80 )
  circle(x,80,60)
  for (let x = -160; x <= width; x += 80 )
  circle(x,160,60)

  
  circle(-160,frameCount,60)
  circle(160,frameCount,60)
  circle(frameCount,160,60)
  circle(frameCount,-160,60)
  
}