# MiniX 2
## Geometric emoji

Runme: https://thelmasif.v5.gitlab.io/aesthetic-programming/MiniX2/
![](Minix2image.png)
### Describe 

In my MiniX 2 with the theme of geometric emoji, I had the idea of making my favorite emojis into one project. What you see at the start is just a regular smiley face but when you press the mouse it changes expression, makes a kissy face and a peace sign with the hand. To execute the idea, I had to discover a few new syntaxes, including mouse pressed, push&pop and rotate. The hardest part of this MiniX was making the peace sign hand. I had to find out how to make a shape that looks like a finger which I did by using a rounded rectangle, which I looked at in the p5js references. Then I had to rotate the fingers which I did by isolating the two shapes of the two erect fingers and then changing the coordinate code until I was happy with the location of them. Then to make the palm I used two circles and for the bent fingers I used a rounded rectangle again. But then I realised, because I was using the “nostroke” syntax to make the design look more clean the bent fingers would get lost in the other shape unless I created dimension by changing the color of the hand to a darker color. 


### Reflect

I think my MiniX is fun to look at and interact with, it has a certain mood to it with the emojis. Also it’s a familiar look and really related to the emojis we see daily on our phones. I chose thesee emojis because they give off good vibes and represent peace and love which is also what I named my sketch (“peace&love”). I think it was good to have a theme set for the minix “geometric emoji” that made it easier to think of ideas than with no theme, there’s so many options on what to make, it can be confusing. 
What I would like to have done differently would maybe be to do shadows behind the shapes, for example with the fingers. I think that maybe it would have looked better than how I did it with different colors of the hand and the bent fingers. Other than that I am very happy with my second minix, I think it was very fun to make, even if it was a little frustrating at times when some things did not work. But I was able to execute the idea I had in my mind at the start and i’m really proud of that. I feel like it’s a good progress since last week and I can’t wait to get even better at making these. 

### References 
Rounded rectangle: https://p5js.org/reference/#/p5/rect

Push&pop and rotate: https://www.youtube.com/watch?v=o9sgjuh-CBM

