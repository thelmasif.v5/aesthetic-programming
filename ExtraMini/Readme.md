# Extra MiniX

<img src="Extramini.jpg" width="300" height="300"> <img src="Extramini2.jpg" width="300" height="300">

Here is a little thing I did where I experimented with mouseX, mouseY and random. 
I got the idea for this around the time that we did miniX4 and thought I would include it as an extra.

#### RunMe: https://thelmasif.v5.gitlab.io/aesthetic-programming/ExtraMini/

How it works is that when you hover the mouse over the volcano it starts to shake, then when you click on it it "errupts"
