let mousePush = false;
let video;
let button;
let Img;


function preload(){
  Img=loadImage('YesCookiemonster.png')
  
 } 
function setup() {
  createCanvas(1000, 800);
  
  video = createCapture(VIDEO);
  video.size(320,240);
  
      //button
  button = createButton('ACCEPT')
  button.position(550,380);
  button.size(150,70)  

}
  
  function mousePressed() {
  mousePush = true;
  let d=dist(mouseX,mouseY,50,50);
  if (d < 200){
    background(255);

}
  }

function draw() {
  background(255);
  if (!mousePush){ 

  stroke(2);
  fill(255)
  rect(150,120,700,400,10);
  
  //cookie
  noStroke();
  fill(200,150,100)
  circle(300,300,200)
  //bite
  fill(255)
  circle(400,300,50)
  circle(370,263,60)
  circle(350,230,50)
  //crumbs
  fill(200,150,100)
  circle (400,250,15)
  circle (360,230,10)
  circle (360,260,10)
  //chocolate chips
  fill(130,80,30)
  circle(270,300,30)
  circle(235,350,28)
  circle(300,360,20)
  circle(290,240,20)
  circle(240,260,15)
  circle(340,330,25)
  
  //text
  fill(0)
  textStyle(BOLD);
  textSize(25)
  text('This program uses cookies...', 450,250)
  textSize(22)
  text('Are you sure you want to accept?',450,300)
  

}else{
  video = createCapture(VIDEO);
  video.size(320,240)
  image(video,320,240)
  image(Img,60,10)
  textSize(50)
  text('YOU STOLE MY COOKIES!',150,300)
  text('Now I have all your data',150,550)
  removeElements();

    
}
  }

 