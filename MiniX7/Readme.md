## MiniX 7 - Revisit the past

RunMe: https://thelmasif.v5.gitlab.io/aesthetic-programming/MiniX7/

<img src="Minix7.jpg" width="300" height="300"> <img src="Minix7.2.jpg" width="300" height="300">

### Changes
Cleaner code, cleaner look, new colors. 

I decided to do some changes to MiniX 3 as I felt it was not successful in being a throbber, like the assignment stated it should be. The instructors gave me feedback and suggested that it would’ve been fun if I had made it a little bit more different from my inspiration. So I’ve made it a different color, green and blue-ish light and I've made it squares instead of circles. It bothers me that it still doesn’t go all the way through but it has something to do with the locX locY that I used for the light effect I think. But still I think it looks good and accomplished the assignment of it being a throbber because now I successfully added a for loop for it to repeat itself. In the original one the circles just went out of the screen and were never to be seen again and that had to be fixed.

### My work in perspective of aesthetic programming

I feel like I am very good at visualising what I want to do and I am always full of ideas, but sometimes it takes a lot of time to figure out how to execute them but it’s a good feeling when you accomplish it, even when the result isn’t exactly how you visualised it. With time you get better at thinking in code in relation to your ideas and how to make them come to live with coding.
Now looking back at my first miniX, not even knowing what a syntax was back then until now, I am very impressed with my improvement throughout the course. I feel like I've gained so much knowledge in writing and reading code and developed a new kind of thinking. Like it says on the preface of Aesthetic programming book growth in “computational thinking”. It’s like learning a new language and getting better and better at writing in that language. It is a useful tool and cultural practice and “a way of thinking and doing in the world”. In my work I emphasise understanding what I am putting in the code.

