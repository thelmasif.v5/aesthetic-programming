# MiniX 3 - Thelma 

RunMe: https://thelmasif.v5.gitlab.io/aesthetic-programming/MiniX3/

![](Minix3.gif.gif)

## Inspiration:
I was very inspired by “loading” symbols which we spoke a little bit about in class and wanted to create one. The main inspiration came from an Icelandic website called www.island.is which is basically like Denmark’s nemID or eboks. I had to do something on there the other day and they had a loading sign that looked like this: 
<img src="Inspoimage.jpg" width="120" height="100">

It had 3x3 circles in fun colors and the circles combined into each other in circles. I really liked the look of this loading sign, thought it looks modern and interesting and I wanted to create something similar. 

## How I made it:
I started by using the “for loop” syntax we learned in class to make a line of circles within the range of the canvas. I just wanted to have 5 circles in each row so the syntax I used was “for (let x = -160; x <= width; x += 80 ) circle(x,-160,60)” and I repeated it five times so it would be a 5x5 grid. I tried to look up an easier way to do this but it did not work, so this had to do. 

I liked the ombré or gradient effect that the inspo design had so I looked up on p5js references and examples how I could do that. I found a light effect that makes it appear as if the mouse is a flashlight shedding light on the circles. I really wanted to use that syntax, so for it to work I had to add WEBGL to the function setup in canvas. That is because this is a function from “Web Graphics Library” that renders interactive 3D and 2D graphics. This was the syntax I used for this function: ambientLight(50);
  directionalLight(255, 0, 0, 2, 2, 0);
  pointLight(0, 0, 255, locX, locY, 100);

The thing I found the hardest to execute and the one thing that did not come out the way I did was the movement of the circles combining into each other. I wanted it to look more like drops and make them “droop” into each other but with a lot of research I could not find a reference or an example that could help me make that happen. So I used the syntax “frameCount” that I found in an instructional video on youtube. I made four more circles with framecount so they move in the same line as the still circles.

I found that with this MiniX it was very frustrating because I had like a vision in my head of what I wanted the end result to look like. The things I would have liked to do differently was the morph of the circles and to make the sketch loop on repeat. But with the limited knowledge I have and the timeframe, it was not executable the way I wanted it to. Regardless I am happy with the result and think it looks good. 


New syntax’s explored:
For loop, framecount, material, light,


### References:
What is Webgl?: https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API

FrameCount: https://youtu.be/fpD8h6x5oBk

Material and lights: https://p5js.org/examples/3d-multiple-lights.html
