## MiniX 1 - Thelma

Runme: https://thelmasif.v5.gitlab.io/aesthetic-programming/MiniX1/ 

![](MiniX1_image.png)


### BEGINNER’S STRUGGLES

It’s easier said than done to set up programming systems on a 2014 macbook air. For a year now I’ve been using an Ipad pro with a keyboard and an apple pen which has allowed me to do everything a macbook does and more and I have been absolutely loving it… Until now. When it came to downloading the Atom programme I realised that was not possible on an Ipad. So I brought my old computer up from the dead. It was like a snowball of inconvenience that started rolling. First I had to update chrome, then clear up space for downloading. Then when I finally had everything downloaded, the atom app did not work. It just showed a bunch of question marks instead of the P5js code. I went to the shut up and code class on friday and Jakob tried to help me with what was wrong with it. He couldn’t figure it out so then he tried to download another coding system called “Visual studio code”. We waited for that to download but then when we tried opening it, it didn’t support the software update version on it. I wanted to throw the computer into the ocean. But Jakob suggested that I would make my MiniX on the web editor on P5js until I figure out what to do. 

### PROCESS OF THE MINIX1

After all the struggles with getting the programs to work, and less time on my hands, in this MiniX I just wanted to emphasise that I understood the basics of what I was doing and only focused on a few things in the start. I feel like I am back in kindergarten learning how to do basic things, like draw a sun and a house and that is what I did. Except there is a lot more behind this sketch than it looks. 
On the web editor in P5js, when you create a new sketch it comes with a preset of a canvas 400x400, and I kept it that way. I started by testing out how the RGB color system works by editing the numbers of the background color. I found a good blue color with the “R” set to 0, “G” set to 100 and “B” set to 250. Then I just started with the basics of doing an ellipse for my sun. I had to google how to get a yellow color because it is as obvious how to do that with the RGB as it is with red, green and blue. Then it was time to do a house. A house consists of a few different elements that I added in, I wanted to keep it simple for me to understand it better. I did a big rectangle for the base, and then tried to find the right coordinates for the house to be in the middle. It was kind of confusing at first with the numbers. I ended up with “(125, 250, 140, 140)” with the rectangle being 140x140 in height and length and then 125 (x), 250 (y) being the coordinates. 
Then I added a roof with a triangle code. That one was a little more complicated because it has more numbers in the bracket than the rectangle. The numbers indicate three points of x and three points of y. The syntax: triangle(x1, y1, x2, y2, x3, y3). I first had to locate the first point of x, then y and then second point and so on. This took a little while to make it sit on top of the house, but when I got the hang of it, it made sense. 
I am really happy with the turquoise color on the roof, which is the result of “G”: 255 and “B”: 200, almost equal parts of green and blue. 
Then I added a rectangle for the door, chimney and the grass. With the same code as the red square, just changed the size, coordinates and color codes. 

I was unhappy with the lines around the elements so Jakob advised me to search up nostroke in the references. I put in the “noStroke();” so the lines would go away and it looked so much better. 
I am really proud of this very first interaction of programming of mine but I hope to do better on the next MiniX now that I know how this works and have more time and help to do it. I did much better than my expectations and if I keep improving this fast, I could get very good at coding in just a few weeks. 


## References 
I found how to code the 2D primitives (shapes) on P5js.org/reference/ 
